/*jslint browser, maxlen: 80 */
/*global PHONE_CHECKER, fetch */
/* eslint-env browser */
/* eslint
    strict: "off",
    fp/no-mutation: "off",
    fp/no-mutating-methods: "off",
    max-statements: "off"
*/

(function main() {
    "use strict";

    const backendUrl = `//${location.host}:8080`;

    const UNAUTHORIZED = 401;

    // DOM elements
    const usernameInput = document.querySelector("[name=\"username\"]");
    const passwordInput = document.querySelector("[name=\"password\"]");
    const loginFrom = document.querySelector(".js-login-form");
    const logoutButton = document.querySelector(".js-log-out");
    const phoneInput = document.querySelector("[name=\"phone\"]");
    const addPhoneForm = document.querySelector(".js-add-phone-form");
    const phonesElement = document.querySelector(".js-phones");

    const {showErrorMessage} = PHONE_CHECKER;

    // Mutable state
    const state = Object.assign(
        {
            phones: [],
            isAuthorized: false,
            token: undefined
        },
        localStorage.state && JSON.parse(localStorage.state)
    );

    function render() {
        loginFrom.classList.toggle("hidden", state.isAuthorized);
        addPhoneForm.classList.toggle("hidden", !state.isAuthorized);
        logoutButton.classList.toggle("hidden", !state.isAuthorized);

        if (state.isAuthorized) {
            usernameInput.value = "";
            passwordInput.value = "";
        }

        phonesElement.innerHTML = state.phones
            .map(
                (phone) => `
<tr>
    <td>
        ${phone.phone}
    </td>
    <td>
        <button
            type="button"
            class="js-remove button-danger"
            data-id="${phone.id}"
        >
            Remove
        </button>
    </td>
</tr>
`
            ).join("");
    }

    function getPhones(phones) {
        state.phones = phones;
        render();
    }

    function addPhone(phone) {
        state.phones.push(phone);
        render();
    }

    function removePhone(id) {
        state.phones = state.phones.filter((phone) => phone.id !== Number(id));
        render();
    }

    function logOut() {
        state.isAuthorized = false;
        state.phones = [];
        localStorage.state = JSON.stringify({isAuthorized: state.isAuthorized});
        render();
    }

    function makeResponseHandler(successHandler) {
        return function handleResponse(response) {
            if (response.status === UNAUTHORIZED) {
                return logOut();
            }

            response
                .json()
                .then(
                    response.ok
                        ? successHandler
                        : showErrorMessage
                )
                .catch(showErrorMessage);
        };
    }

    const handleGetPhonesResponse = makeResponseHandler(getPhones);

    const handleAddPhoneResponse = makeResponseHandler(addPhone);

    function fetchPhones() {
        fetch(`${backendUrl}/phones`, {
            headers: {authorization: `Bearer ${state.accessToken}`}
        })
            .then(handleGetPhonesResponse)
            .catch(showErrorMessage);
    }

    function logIn({access_token: accessToken}) {
        state.isAuthorized = true;
        state.accessToken = accessToken;
        localStorage.state = JSON.stringify({
            isAuthorized: state.isAuthorized,
            accessToken: state.accessToken
        });
        fetchPhones();
        render();
    }

    function handlePhoneSubmit(event) {
        event.preventDefault();

        fetch(`${backendUrl}/phones`, {
            method: "POST",
            headers: {
                "content-type": "application/json",
                authorization: `Bearer ${state.accessToken}`
            },
            body: JSON.stringify({phone: phoneInput.value})
        })
            .then(handleAddPhoneResponse)
            .catch(showErrorMessage);
    }

    function handleLogin(event) {
        event.preventDefault();

        function handleResponse(response) {
            return response.json().then(
                response.ok
                    ? logIn
                    : (err) => showErrorMessage({
                        message: err.error_description
                    })
            );
        }

        fetch(`${backendUrl}/oauth2/token`, {
            method: "POST",
            headers: {"content-type": "application/x-www-form-urlencoded"},
            body: (
                "grant_type=password&"
                + `username=${usernameInput.value}&`
                + `password=${passwordInput.value}&`
                + "client_id=phoneChecker&"
                + "client_secret=false"
            )
        })
            .then(handleResponse)
            .catch(showErrorMessage);
    }

    function handleRemove(event) {
        function handleResponse(id, response) {
            if (response.status === UNAUTHORIZED) {
                return logOut();
            }

            return response.ok
                ? removePhone(id)
                : response.json().then(showErrorMessage);
        }

        if (event.target.matches(".js-remove")) {
            const {id} = event.target.dataset;

            fetch(`${backendUrl}/phones/${id}`, {
                method: "DELETE",
                headers: {authorization: `Bearer ${state.accessToken}`}
            })
                .then((response) => handleResponse(id, response))
                .catch(showErrorMessage);
        }
    }

    function handleLogout() {
        fetch(`${backendUrl}/oauth2/token`, {
            method: "DELETE",
            headers: {authorization: `Bearer ${state.accessToken}`}
        })
            .then(logOut)
            .catch(showErrorMessage);
    }

    // Attach DOM event handlers
    loginFrom.addEventListener("submit", handleLogin);
    addPhoneForm.addEventListener("submit", handlePhoneSubmit);
    document.addEventListener("click", handleRemove);
    logoutButton.addEventListener("click", handleLogout);

    // Init
    fetchPhones();
}());
