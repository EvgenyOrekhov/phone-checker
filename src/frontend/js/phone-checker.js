/*jslint browser, maxlen: 80 */
/*global PHONE_CHECKER, fetch */
/* eslint-env browser */
/* eslint strict: "off" */

(function main() {
    "use strict";

    const backendUrl = `//${location.host}:8080`;

    // DOM elements
    const input = document.querySelector("[name=\"phone\"]");
    const form = document.querySelector(".js-form");

    const {showMessage, showErrorMessage} = PHONE_CHECKER;

    function handleResponse(response) {
        return response.json().then(
            response.ok
                ? (doesPhoneExist) => showMessage(
                    doesPhoneExist
                        ? "The phone is present in the DB"
                        : "The phone is not present in the DB"
                )
                : showErrorMessage
        );
    }

    form.addEventListener("submit", function checkPhone(event) {
        event.preventDefault();

        fetch(`${backendUrl}/phones/${encodeURIComponent(input.value)}`)
            .then(handleResponse)
            .catch(showErrorMessage);
    });
}());
