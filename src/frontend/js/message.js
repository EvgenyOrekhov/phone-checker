/*jslint browser, maxlen: 80 */
/* eslint-env browser */
/* eslint
    strict: "off",
    fp/no-mutation: "off",
    max-statements: "off"
*/

// eslint-disable-next-line no-unused-vars
const PHONE_CHECKER = (function main() {
    "use strict";

    // DOM elements
    const inputs = document.querySelectorAll("input");
    const messageElement = document.querySelector(".js-message");

    // Mutable state
    const state = {message: ""};

    function render() {
        messageElement.innerText = state.message;
    }

    function showMessage(message) {
        state.message = message;
        render();
    }

    function hideMessage() {
        state.message = "";
        render();
    }

    function showErrorMessage({message}) {
        showMessage(message);
    }

    // Attach DOM event handlers
    inputs.forEach((input) => input.addEventListener("input", hideMessage));
    messageElement.addEventListener("click", hideMessage);

    return Object.freeze({
        showMessage,
        showErrorMessage,
        hideMessage
    });
}());
