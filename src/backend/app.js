/*jslint es6, node, maxlen: 80 */

"use strict";

const config = require("config");

const makeServer = require("./server");

const {hostname, port} = config.server;

const server = makeServer(config);

server.listen(port, hostname, function logUrl() {
    // eslint-disable-next-line no-console
    console.log(`Server running at http://${hostname}:${port}/`);
});
