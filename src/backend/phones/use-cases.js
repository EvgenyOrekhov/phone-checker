/*jslint node, maxlen: 80 */

"use strict";

const {PhoneNumberFormat, PhoneNumberUtil} = require("google-libphonenumber");

const phoneUtil = PhoneNumberUtil.getInstance();

function formatPhone(phone) {
    try {
        const formattedPhone = phoneUtil.format(
            phoneUtil.parse(phone),
            PhoneNumberFormat.E164
        );

        return Promise.resolve(formattedPhone);
    } catch (err) {
        return Promise.reject(err);
    }
}

module.exports = function makeUseCases({gateways}) {
    return {
        addPhone(phone) {
            function checkExistenceAndAddPhone(formattedPhone) {
                return gateways
                    .getPhone({phone: formattedPhone})
                    .then(function addPhoneIfDoesntExist(retrievedPhone) {
                        return retrievedPhone === undefined
                            ? gateways.addPhone(formattedPhone)
                            : Promise.reject(
                                new Error("The phone already exists")
                            );
                    });
            }

            return formatPhone(phone).then(checkExistenceAndAddPhone);
        },

        removePhone: (id) => gateways
            .getPhone({id})
            .then(function removePhoneIfExists(retrievedPhone) {
                return retrievedPhone === undefined
                    ? undefined
                    : gateways.removePhone(id);
            }),

        checkPhone: (phone) => formatPhone(phone)
            .then(
                (formattedPhone) => gateways.getPhone({phone: formattedPhone})
            )
            .then(Boolean),

        getPhones: gateways.getPhones
    };
};
