/*jslint node, maxlen: 80 */

"use strict";

function getFirstRow({rows}) {
    return rows[0];
}

module.exports = function makeGateways(pool) {
    return {
        getPhones: () => pool
            .query("SELECT * FROM phones;")
            .then((result) => result.rows),

        addPhone: (newPhone) => pool
            .query(
                "INSERT INTO phones(phone) VALUES ($1) RETURNING *;",
                [newPhone]
            )
            .then(getFirstRow),

        getPhone({id, phone}) {
            return pool
                .query(
                    ...id
                        ? [
                            "SELECT * FROM phones WHERE id = $1;",
                            [id]
                        ]
                        : [
                            "SELECT * FROM phones WHERE phone = $1;",
                            [phone]
                        ]
                )
                .then(getFirstRow);
        },

        removePhone: (id) => pool
            .query(
                "DELETE FROM phones WHERE id = $1",
                [id]
            )
            .then(() => undefined)
    };
};
