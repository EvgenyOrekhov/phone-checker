#!/bin/sh

# shellcheck disable=SC2016

set -eu

execute() {
    psql \
        --variable ON_ERROR_STOP=1 \
        --username "$POSTGRES_USER" \
        --command "$@"
}

CREATE_TABLES='
CREATE TABLE phones (
    "id" serial PRIMARY KEY,
    "phone" varchar(15) NOT NULL
);

CREATE TABLE users (
    "id" serial PRIMARY KEY,
    "username" varchar(255) NOT NULL UNIQUE,
    "passwordHash" varchar(60) NOT NULL
);

CREATE TABLE oauth2_clients (
    "id" varchar(255) PRIMARY KEY,
    "grants" varchar(255) NOT NULL
);

CREATE TABLE oauth2_tokens (
    "id" serial PRIMARY KEY,
    "accessToken" varchar(40) NOT NULL,
    "accessTokenExpiresAt" varchar(30) NOT NULL,
    "clientId" varchar(255) NOT NULL REFERENCES oauth2_clients ("id") ON DELETE CASCADE,
    "userId" integer NOT NULL REFERENCES users ("id") ON DELETE CASCADE
);

INSERT INTO users ("username", "passwordHash") VALUES (
    '"'"'admin'"'"',
    '"'"'$2a$10$rwCNlwomhAgePqzDRoP4dOnqvgkGg9AOpysZs8L0/JPlWqUExQckO'"'"'
);

INSERT INTO oauth2_clients ("id", "grants") VALUES (
    '"'"'phoneChecker'"'"',
    '"'"'["password"]'"'"'
);
'

execute 'CREATE DATABASE test;'

execute "$CREATE_TABLES" --dbname postgres

execute "$CREATE_TABLES" --dbname test
