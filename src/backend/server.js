/*jslint node, maxlen: 80 */
/* eslint promise/no-callback-in-promise: "off" */

"use strict";

const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const HTTPStatus = require("http-status");
const {Pool} = require("pg");
const OAuthServer = require("express-oauth-server");
const bearerToken = require("express-bearer-token");

const makePhoneGateways = require("./phones/gateways");
const makePhoneUseCases = require("./phones/use-cases");

const makeAccountGateways = require("./accounts/gateways");
const makeAccountUseCases = require("./accounts/use-cases");

const app = express();

app.use([
    bodyParser.urlencoded({extended: true}),
    bodyParser.json(),
    cors(),
    bearerToken()
]);

module.exports = function makeServer({db}) {
    const pool = new Pool(db);

    const accountUseCases = makeAccountUseCases({
        gateways: makeAccountGateways(pool)
    });

    const oauth = new OAuthServer({
        grants: ["password"],
        debug: true,
        model: accountUseCases
    });

    (function mountOAuthEndpoints() {
        app.post("/oauth2/token", oauth.token());

        app.delete(
            "/oauth2/token",
            oauth.authenticate(),
            function logOut(req, res, next) {
                accountUseCases
                    .logOut(req.token)
                    .then(() => res.status(HTTPStatus.NO_CONTENT).send())
                    .catch(next);
            }
        );
    }());

    (function mountPhoneEndpoints() {
        const phoneUseCases = makePhoneUseCases({
            gateways: makePhoneGateways(pool)
        });

        app.get("/phones/:phone", function getPhones(req, res) {
            phoneUseCases
                .checkPhone(req.params.phone)
                .then((doesPhoneExists) => res.send(doesPhoneExists))
                .catch(
                    (err) => res
                        .status(HTTPStatus.BAD_REQUEST)
                        .send({message: err.message})
                );
        });

        app.get(
            "/phones",
            oauth.authenticate(),
            function getPhones(ignore, res, next) {
                phoneUseCases
                    .getPhones()
                    .then((phones) => res.send(phones))
                    .catch(next);
            }
        );

        app.post(
            "/phones",
            oauth.authenticate(),
            function addPhone(req, res) {
                phoneUseCases
                    .addPhone(req.body.phone)
                    .then((phone) => res.status(HTTPStatus.CREATED).send(phone))
                    .catch(
                        (err) => res
                            .status(HTTPStatus.BAD_REQUEST)
                            .send({message: err.message})
                    );
            }
        );

        app.delete(
            "/phones/:id",
            oauth.authenticate(),
            function removePhone(req, res, next) {
                phoneUseCases
                    .removePhone(req.params.id)
                    .then(() => res.status(HTTPStatus.NO_CONTENT).send())
                    .catch(next);
            }
        );
    }());

    return app;
};
