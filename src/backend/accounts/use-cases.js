/*jslint node, maxlen: 80 */

"use strict";

const bcrypt = require("bcrypt");

function parseAccessTokenExpiresAt(accessToken) {
    return Object.assign({}, accessToken, {
        accessTokenExpiresAt: new Date(accessToken.accessTokenExpiresAt)
    });
}

module.exports = function makeUseCases({gateways}) {
    return {
        getUser(username, password) {
            function comparePasswordsAndReturnUser(user) {
                return user
                    ? bcrypt
                        .compare(password, user.passwordHash)
                        .then(function returnUserIfPasswordValid(
                            isPasswordValid
                        ) {
                            return isPasswordValid
                                ? user
                                : undefined;
                        })
                    : undefined;
            }

            return gateways
                .getUser({username})
                .then(comparePasswordsAndReturnUser);
        },

        saveToken: (accessToken, client, user) => Promise
            .all([
                gateways.saveToken(accessToken, client, user),
                gateways.getClient(client.id),
                gateways.getUser(user)
            ])
            .then(function merge([
                accessTokenFromDb,
                clientFromDb,
                userFromDb
            ]) {
                return Object.assign(
                    {
                        client: clientFromDb,
                        user: userFromDb
                    },
                    parseAccessTokenExpiresAt(accessTokenFromDb)
                );
            }),

        getAccessToken(accessToken) {
            function getAndMergeUser(accessTokenFromDb) {
                return accessTokenFromDb
                    ? gateways
                        .getUserById(accessTokenFromDb.userId)
                        .then(
                            (user) => Object.assign(
                                {user},
                                parseAccessTokenExpiresAt(accessTokenFromDb)
                            )
                        )
                    : undefined;
            }

            return gateways
                .getAccessToken(accessToken)
                .then(getAndMergeUser);
        },

        getClient: gateways.getClient,

        logOut: gateways.removeAccessToken
    };
};
