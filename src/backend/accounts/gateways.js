/*jslint node, maxlen: 80 */

"use strict";

const R = require("ramda");

function getFirstRow({rows}) {
    return rows[0];
}

function evolveIfNotNil(transformations) {
    return R.unless(R.isNil, R.evolve(transformations));
}

module.exports = function makeGateways(pool) {
    return {
        getUser({username}) {
            return pool
                .query(
                    "SELECT * FROM users WHERE username = $1;",
                    [username]
                )
                .then(getFirstRow);
        },

        getUserById: (id) => pool
            .query(
                "SELECT * FROM users WHERE id = $1;",
                [id]
            )
            .then(getFirstRow),

        getClient: (id) => pool
            .query(
                "SELECT * FROM oauth2_clients WHERE id = $1;",
                [id]
            )
            .then(getFirstRow)
            .then(evolveIfNotNil({grants: JSON.parse})),

        saveToken({accessToken, accessTokenExpiresAt}, client, user) {
            return pool
                .query(
                    `
INSERT INTO oauth2_tokens (
    "accessToken",
    "accessTokenExpiresAt",
    "clientId",
    "userId"
) VALUES ($1, $2, $3, $4) RETURNING *;
`,
                    [accessToken, accessTokenExpiresAt, client.id, user.id]
                )
                .then(getFirstRow);
        },

        getAccessToken: (accessToken) => pool
            .query(
                "SELECT * FROM oauth2_tokens WHERE \"accessToken\" = $1;",
                [accessToken]
            )
            .then(getFirstRow),

        removeAccessToken: (accessToken) => pool.query(
            "DELETE FROM oauth2_tokens WHERE \"accessToken\" = $1;",
            [accessToken]
        )
    };
};
