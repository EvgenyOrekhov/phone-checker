/*jslint node, maxlen: 80 */
/*eslint func-names: "off" */

"use strict";

const {
    beforeEach,
    afterEach,
    describe,
    it
} = require("mocha");

const {expect} = require("chai");

const {Pool} = require("pg");

const makeGateways = require("../../../src/backend/accounts/gateways");

const config = {
    user: "postgres",
    host: "postgres",
    database: "test"
};

const pool = new Pool(config);

const gateways = makeGateways(pool);

function resetTables() {
    return pool.query(
        `
TRUNCATE TABLE users CASCADE;
ALTER SEQUENCE users_id_seq RESTART WITH 1;
TRUNCATE TABLE oauth2_clients CASCADE;
TRUNCATE TABLE oauth2_tokens CASCADE;
ALTER SEQUENCE oauth2_tokens_id_seq RESTART WITH 1;
`
    );
}

describe("backend/accounts/gateways", function () {
    beforeEach(
        () => resetTables().then(
            () => Promise.all([
                pool.query(
                    `
INSERT INTO users ("username", "passwordHash") VALUES ($1, $2), ($3, $4);
`,
                    ["user1", "passwordHash1", "user2", "passwordHash2"]
                ),

                pool.query(
                    `
INSERT INTO oauth2_clients ("id", "grants") VALUES ($1, $2), ($3, $4);
`,
                    [
                        "client1",
                        JSON.stringify(["password"]),
                        "client2",
                        JSON.stringify(["password"])
                    ]
                )
            ])
        )
    );

    afterEach(resetTables);

    describe("getUser", function () {
        it("should get a user from the DB", function () {
            return Promise.all([
                gateways
                    .getUser({username: "user1"})
                    .then(
                        (user) => expect(user).to.eql({
                            id: 1,
                            username: "user1",
                            passwordHash: "passwordHash1"
                        })
                    ),

                gateways
                    .getUser({username: "user2"})
                    .then(
                        (user) => expect(user).to.eql({
                            id: 2,
                            username: "user2",
                            passwordHash: "passwordHash2"
                        })
                    )
            ]);
        });
    });

    describe("getClient", function () {
        it("should get an OAuth2 client from the DB", function () {
            return Promise.all([
                gateways
                    .getClient("client1")
                    .then(
                        (client) => expect(client).to.eql({
                            id: "client1",
                            grants: ["password"]
                        })
                    ),

                gateways
                    .getClient("client2")
                    .then(
                        (client) => expect(client).to.eql({
                            id: "client2",
                            grants: ["password"]
                        })
                    )
            ]);
        });

        it("should return undefined if the client is not found", function () {
            return gateways
                .getClient("nonexistent")
                .then((client) => expect(client).to.eql(undefined));
        });
    });

    describe("saveToken", function () {
        it("should save an OAuth2 token to the DB", function () {
            return gateways
                .saveToken(
                    {
                        accessToken: "token",
                        accessTokenExpiresAt: "2017-12-02T13:42:05.057+00:00"
                    },
                    {id: "client2"},
                    {id: 2}
                )
                .then(
                    (accessToken) => expect(accessToken).to.eql({
                        id: 1,
                        accessToken: "token",
                        accessTokenExpiresAt: "2017-12-02T13:42:05.057+00:00",
                        clientId: "client2",
                        userId: 2
                    })
                )
                .then(() => pool.query("SELECT * FROM oauth2_tokens;"))
                .then(function ({rows}) {
                    return expect(rows).to.eql([
                        {
                            id: 1,
                            accessToken: "token",
                            accessTokenExpiresAt: (
                                "2017-12-02T13:42:05.057+00:00"
                            ),
                            clientId: "client2",
                            userId: 2
                        }
                    ]);
                });
        });
    });

    describe("getAccessToken", function () {
        it("should get the access token from the DB", function () {
            return gateways
                .saveToken(
                    {
                        accessToken: "token",
                        accessTokenExpiresAt: "2017-12-02T13:42:05.057+00:00"
                    },
                    {id: "client2"},
                    {id: 2}
                )
                .then(() => gateways.getAccessToken("token"))
                .then(
                    (accessToken) => expect(accessToken).to.eql({
                        id: 1,
                        accessToken: "token",
                        accessTokenExpiresAt: "2017-12-02T13:42:05.057+00:00",
                        clientId: "client2",
                        userId: 2
                    })
                );
        });

        it(
            "should return undefined if the access token is not found",
            function () {
                return gateways
                    .getAccessToken("nonexistent")
                    .then(
                        (accessToken) => expect(accessToken).to.eql(undefined)
                    );
            }
        );
    });
});
