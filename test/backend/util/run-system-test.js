/*jslint node, maxlen: 80 */
/*eslint func-names: "off" */

"use strict";

const {describe, before, after} = require("mocha");

const {Pool} = require("pg");

const makeServer = require("../../../src/backend/server");

const config = {
    db: {
        user: "postgres",
        host: "postgres",
        database: "test"
    }
};

const pool = new Pool(config.db);

const server = makeServer(config);

module.exports = function runSystemTest(suiteName, test) {
    describe(suiteName, function () {
        before(
            () => pool.query(
                `
TRUNCATE TABLE oauth2_tokens CASCADE;
ALTER SEQUENCE oauth2_tokens_id_seq RESTART WITH 1;
TRUNCATE TABLE oauth2_clients CASCADE;
TRUNCATE TABLE users CASCADE;
ALTER SEQUENCE users_id_seq RESTART WITH 1;
TRUNCATE TABLE phones;
ALTER SEQUENCE phones_id_seq RESTART WITH 1;
INSERT INTO oauth2_clients ("id", "grants") VALUES (
    'phoneChecker',
    '["password"]'
);
INSERT INTO users ("username", "passwordHash") VALUES (
    'admin',
    '$2a$10$rwCNlwomhAgePqzDRoP4dOnqvgkGg9AOpysZs8L0/JPlWqUExQckO'
);
INSERT INTO oauth2_tokens (
    "accessToken",
    "accessTokenExpiresAt",
    "clientId",
    "userId"
) VALUES (
    'token',
    '2050-12-02T13:42:05.057+00:00',
    'phoneChecker',
    1
);
    `
            )
        );

        after(
            () => pool.query(
                `
TRUNCATE TABLE oauth2_tokens CASCADE;
ALTER SEQUENCE oauth2_tokens_id_seq RESTART WITH 1;
TRUNCATE TABLE oauth2_clients CASCADE;
TRUNCATE TABLE users CASCADE;
ALTER SEQUENCE users_id_seq RESTART WITH 1;
TRUNCATE TABLE phones;
ALTER SEQUENCE phones_id_seq RESTART WITH 1;
    `
            )
        );

        test({server});
    });
};
