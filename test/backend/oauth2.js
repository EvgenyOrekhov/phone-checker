/*jslint node, maxlen: 80 */
/*eslint func-names: "off" */

"use strict";

const {it} = require("mocha");
const chai = require("chai");
const chaiHttp = require("chai-http");

const querystring = require("querystring");

const HTTPStatus = require("http-status");

const runSystemTest = require("./util/run-system-test");

const {expect} = chai;

chai.use(chaiHttp);

runSystemTest("OAuth2", function ({server}) {
    it(
        "should grant an access token using the \"password\" grant type",
        function () {
            return chai
                .request(server)
                .post("/oauth2/token")
                .send(
                    querystring.stringify({
                        /* eslint-disable camelcase */
                        grant_type: "password",
                        username: "admin",
                        password: "admin",
                        client_id: "phoneChecker",
                        client_secret: false
                        /* eslint-enable camelcase */
                    })
                )
                .then(function (res) {
                    return expect(res.body).to.have.keys([
                        "access_token",
                        "expires_in",
                        "token_type"
                    ]);
                });
        }
    );

    it(
        "should not grant an access token if the password is invalid",
        function () {
            return chai
                .request(server)
                .post("/oauth2/token")
                .send(
                    querystring.stringify({
                        /* eslint-disable camelcase */
                        grant_type: "password",
                        username: "admin",
                        password: "invalid",
                        client_id: "phoneChecker",
                        client_secret: false
                        /* eslint-enable camelcase */
                    })
                )
                .then(
                    (res) => expect(res).to.not.be.ok,
                    (err) => expect(err.response).to.have.status(
                        HTTPStatus.BAD_REQUEST
                    )
                );
        }
    );

    it(
        "should not grant an access token if the user is not found",
        function () {
            return chai
                .request(server)
                .post("/oauth2/token")
                .send(
                    querystring.stringify({
                        /* eslint-disable camelcase */
                        grant_type: "password",
                        username: "nonexistent",
                        password: "admin",
                        client_id: "phoneChecker",
                        client_secret: false
                        /* eslint-enable camelcase */
                    })
                )
                .then(
                    (res) => expect(res).to.not.be.ok,
                    (err) => expect(err.response).to.have.status(
                        HTTPStatus.BAD_REQUEST
                    )
                );
        }
    );

    it(
        "should use authentication",
        function () {
            return Promise.all([
                chai
                    .request(server)
                    .post("/phones")
                    .send({phone: "+79999999999"})
                    .then(
                        (res) => expect(res).to.not.be.ok,
                        (err) => expect(err.response).to.have.status(
                            HTTPStatus.UNAUTHORIZED
                        )
                    ),

                chai
                    .request(server)
                    .get("/phones")
                    .then(
                        (res) => expect(res).to.not.be.ok,
                        (err) => expect(err.response).to.have.status(
                            HTTPStatus.UNAUTHORIZED
                        )
                    ),

                chai
                    .request(server)
                    .delete("/phones/1")
                    .then(
                        (res) => expect(res).to.not.be.ok,
                        (err) => expect(err.response).to.have.status(
                            HTTPStatus.UNAUTHORIZED
                        )
                    )
            ]);
        }
    );

    it(
        "should properly handle nonexistent tokens",
        function () {
            return chai
                .request(server)
                .post("/phones")
                .set("Authorization", "Bearer nonexistent")
                .send({phone: "+79999999999"})
                .then(
                    (res) => expect(res).to.not.be.ok,
                    (err) => expect(err.response).to.have.status(
                        HTTPStatus.UNAUTHORIZED
                    )
                );
        }
    );

    it(
        "should implement logout",
        function () {
            return chai
                .request(server)
                .delete("/oauth2/token")
                .send()
                .then(
                    (res) => expect(res).to.not.be.ok,
                    (err) => expect(err.response).to.have.status(
                        HTTPStatus.UNAUTHORIZED
                    )
                )
                .then(
                    () => chai
                        .request(server)
                        .delete("/oauth2/token")
                        .set("Authorization", "Bearer token")
                        .send()
                )
                .then(
                    (res) => expect(res).to.have.status(HTTPStatus.NO_CONTENT)
                )
                .then(
                    () => chai
                        .request(server)
                        .delete("/oauth2/token")
                        .set("Authorization", "Bearer token")
                        .send()
                )
                .then(
                    (res) => expect(res).to.not.be.ok,
                    (err) => expect(err.response).to.have.status(
                        HTTPStatus.UNAUTHORIZED
                    )
                );
        }
    );
});
