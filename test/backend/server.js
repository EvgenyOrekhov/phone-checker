/*jslint node, maxlen: 80 */
/*eslint func-names: "off" */

"use strict";

const {it} = require("mocha");
const chai = require("chai");
const chaiHttp = require("chai-http");

const HTTPStatus = require("http-status");

const runSystemTest = require("./util/run-system-test");

const {expect} = chai;

chai.use(chaiHttp);

runSystemTest("server", function ({server}) {
    it(
        "should add a new phone",
        function () {
            return chai
                .request(server)
                .post("/phones")
                .set("Authorization", "Bearer token")
                .send({phone: "+79999999999"})
                .then(function (res) {
                    expect(res).to.have.status(HTTPStatus.CREATED);

                    expect(res.body.id).to.be.a("number");

                    return expect(res.body.phone).to.eql("+79999999999");
                });
        }
    );

    it(
        "should not add a new phone if it is invalid",
        function () {
            return chai
                .request(server)
                .post("/phones")
                .set("Authorization", "Bearer token")
                .send({phone: "02"})
                .then(
                    (res) => expect(res).to.not.be.ok,
                    function (err) {
                        expect(err).to.have.status(HTTPStatus.BAD_REQUEST);

                        return expect(err.response.body).to.eql({
                            message: "Invalid country calling code"
                        });
                    }
                );
        }
    );

    it(
        "should get phones",
        function () {
            return chai
                .request(server)
                .get("/phones")
                .set("Authorization", "Bearer token")
                .then(function (res) {
                    expect(res).to.have.status(HTTPStatus.OK);

                    return expect(res.body).to.eql([
                        {
                            id: 1,
                            phone: "+79999999999"
                        }
                    ]);
                });
        }
    );

    it(
        "should remove a phone",
        function () {
            return chai
                .request(server)
                .delete("/phones/1")
                .set("Authorization", "Bearer token")
                .then(function (res) {
                    expect(res).to.have.status(HTTPStatus.NO_CONTENT);

                    return expect(res.body).to.eql({});
                });
        }
    );

    it(
        "should check a phone",
        function () {
            return chai
                .request(server)
                .get("/phones/%2B79999999999")
                .then(function (res) {
                    expect(res).to.have.status(HTTPStatus.OK);

                    return expect(res.body).to.eql(false);
                });
        }
    );

    it(
        "should report an invalid phone when checking",
        function () {
            return chai
                .request(server)
                .get("/phones/02")
                .then(
                    (res) => expect(res).to.not.be.ok,
                    function (err) {
                        expect(err).to.have.status(HTTPStatus.BAD_REQUEST);

                        return expect(err.response.body).to.eql({
                            message: "Invalid country calling code"
                        });
                    }
                );
        }
    );
});
