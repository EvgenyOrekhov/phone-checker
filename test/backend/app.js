/*jslint node, maxlen: 80 */
/*eslint func-names: "off" */

"use strict";

const {describe, it} = require("mocha");
const chai = require("chai");
const chaiHttp = require("chai-http");

chai.use(chaiHttp);

describe("app", function () {
    it(
        "should start the server",
        () => chai
            .request("http://localhost")
            .get("/phones/+79999999999")
            .send()
    );
});
