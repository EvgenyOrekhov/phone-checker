/*jslint node, maxlen: 80 */
/*eslint func-names: "off" */

"use strict";

const {
    beforeEach,
    afterEach,
    describe,
    it
} = require("mocha");

const {expect} = require("chai");

const {Pool} = require("pg");

const makeGateways = require("../../../src/backend/phones/gateways");

const config = {
    user: "postgres",
    host: "postgres",
    database: "test"
};

const pool = new Pool(config);

const gateways = makeGateways(pool);

describe("backend/phones/gateways", function () {
    beforeEach(
        () => pool.query(
            "INSERT INTO phones (phone) VALUES ($1), ($2);",
            ["123", "456"]
        )
    );

    afterEach(
        () => pool.query(
            `
TRUNCATE TABLE phones;
ALTER SEQUENCE phones_id_seq RESTART WITH 1;
`
        )
    );

    describe("getPhones", function () {
        it("should get phones from the DB", function () {
            return gateways
                .getPhones()
                .then(
                    (retrievedPhones) => expect(retrievedPhones).to.eql([
                        {
                            id: 1,
                            phone: "123"
                        },
                        {
                            id: 2,
                            phone: "456"
                        }
                    ])
                );
        });
    });

    describe("addPhone", function () {
        it("should add a new phone to the DB", function () {
            return gateways
                .addPhone("789")
                .then(
                    (addedPhone) => expect(addedPhone).to.eql({
                        id: 3,
                        phone: "789"
                    })
                )
                .then(gateways.getPhones)
                .then(
                    (retrievedPhones) => expect(retrievedPhones).to.eql([
                        {
                            id: 1,
                            phone: "123"
                        },
                        {
                            id: 2,
                            phone: "456"
                        },
                        {
                            id: 3,
                            phone: "789"
                        }
                    ])
                );
        });
    });

    describe("getPhone", function () {
        it("should get a phone from the DB", function () {
            return Promise.all([
                gateways
                    .getPhone({id: 1})
                    .then(
                        (retrievedPhone) => expect(retrievedPhone).to.eql({
                            id: 1,
                            phone: "123"
                        })
                    ),

                gateways
                    .getPhone({phone: "456"})
                    .then(
                        (retrievedPhone) => expect(retrievedPhone).to.eql({
                            id: 2,
                            phone: "456"
                        })
                    )
            ]);
        });
    });

    describe("removePhone", function () {
        it("should remove a phone from the DB", function () {
            const idOfThePhoneToRemove = 2;

            return gateways
                .removePhone(idOfThePhoneToRemove)
                .then(
                    (result) => expect(result).to.eql(undefined)
                )
                .then(gateways.getPhones)
                .then(
                    (retrievedPhones) => expect(retrievedPhones).to.eql([
                        {
                            id: 1,
                            phone: "123"
                        }
                    ])
                );
        });
    });
});
