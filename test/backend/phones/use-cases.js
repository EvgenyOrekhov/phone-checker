/*jslint node, maxlen: 80 */
/*eslint func-names: "off" */

"use strict";

const {describe, it} = require("mocha");
const chai = require("chai");
const chaiSpies = require("chai-spies");

const {expect} = chai;

chai.use(chaiSpies);

const makeUseCases = require("../../../src/backend/phones/use-cases");

describe("backend/phones/use-cases", function () {
    const firstPhone = {
        id: 0,
        phone: "+79999999123"
    };

    const secondPhone = {
        id: 1,
        phone: "+79999999456"
    };

    const phones = [firstPhone, secondPhone];

    const newUnformattedPhone = "+7 (999) 999-97-89";

    const newPhone = {
        id: 2,
        phone: "+79999999789"
    };

    const invalidPhone = "02";

    describe("getPhones", function () {
        it("should get phones", function () {
            const gateways = {
                getPhones: chai.spy(() => Promise.resolve(phones))
            };

            const useCases = makeUseCases({gateways});

            return useCases
                .getPhones()
                .then(
                    (retrievedPhones) => expect(retrievedPhones).to.eql(phones)
                );
        });
    });

    describe("addPhone", function () {
        it("should format and add a new phone", function () {
            const gateways = {
                addPhone: chai.spy(() => Promise.resolve(newPhone)),
                getPhone: chai.spy(() => Promise.resolve(undefined))
            };

            const useCases = makeUseCases({gateways});

            return useCases
                .addPhone(newUnformattedPhone)
                .then(function (addedPhone) {
                    expect(gateways.getPhone)
                        .to.have.been.called.once
                        .with.exactly({phone: newPhone.phone});

                    expect(gateways.addPhone)
                        .to.have.been.called.once
                        .with.exactly(newPhone.phone);

                    return expect(addedPhone).to.eql(newPhone);
                });
        });

        it("should not add a new phone if it already exists", function () {
            const gateways = {
                getPhone: chai.spy(() => Promise.resolve(newPhone))
            };

            const useCases = makeUseCases({gateways});

            return useCases
                .addPhone(secondPhone.phone)
                .then(
                    (addedPhone) => expect(addedPhone).to.not.be.ok,
                    function ({message}) {
                        return expect(message).to.eql(
                            "The phone already exists"
                        );
                    }
                );
        });

        it("should not add a new phone if it is invalid", function () {
            const gateways = {};

            const useCases = makeUseCases({gateways});

            return useCases
                .addPhone(invalidPhone)
                .then(
                    (addedPhone) => expect(addedPhone).to.not.be.ok,
                    function ({message}) {
                        return expect(message).to.eql(
                            "Invalid country calling code"
                        );
                    }
                );
        });
    });

    describe("removePhone", function () {
        it("should remove a phone", function () {
            const gateways = {
                removePhone: chai.spy(() => Promise.resolve(undefined)),
                getPhone: chai.spy(() => Promise.resolve(newPhone))
            };

            const useCases = makeUseCases({gateways});

            return useCases
                .removePhone(newPhone.id)
                .then(function (result) {
                    expect(gateways.getPhone)
                        .to.have.been.called.once
                        .with.exactly({id: newPhone.id});

                    expect(gateways.removePhone)
                        .to.have.been.called.once
                        .with.exactly(newPhone.id);

                    return expect(result).to.eql(undefined);
                });
        });

        it("should be idempotent", function () {
            const gateways = {
                getPhone: chai.spy(() => Promise.resolve(undefined))
            };

            const useCases = makeUseCases({gateways});

            return useCases
                .removePhone(newPhone.id)
                .then((result) => expect(result).to.eql(undefined));
        });
    });

    describe("checkPhone", function () {
        it("should return true if a phone exists", function () {
            const gateways = {
                getPhone: chai.spy(() => Promise.resolve(newPhone))
            };

            const useCases = makeUseCases({gateways});

            return useCases
                .checkPhone(newPhone.phone)
                .then(function (doesPhoneExist) {
                    expect(gateways.getPhone)
                        .to.have.been.called.once
                        .with.exactly({phone: newPhone.phone});

                    return expect(doesPhoneExist).to.eql(true);
                });
        });

        it("should format a phone before checking", function () {
            const gateways = {
                getPhone: chai.spy(() => Promise.resolve(newPhone))
            };

            const useCases = makeUseCases({gateways});

            return useCases
                .checkPhone(newUnformattedPhone)
                .then(function (doesPhoneExist) {
                    expect(gateways.getPhone)
                        .to.have.been.called.once
                        .with.exactly({phone: newPhone.phone});

                    return expect(doesPhoneExist).to.eql(true);
                });
        });

        it("should report an invalid phone", function () {
            const gateways = {
                getPhone: chai.spy(() => Promise.resolve(newPhone))
            };

            const useCases = makeUseCases({gateways});

            return useCases
                .checkPhone(invalidPhone)
                .then(
                    (result) => expect(result).to.not.be.ok,
                    function ({message}) {
                        return expect(message).to.eql(
                            "Invalid country calling code"
                        );
                    }
                );
        });

        it("should return false if a phone does not exist", function () {
            const gateways = {
                getPhone: chai.spy(() => Promise.resolve(undefined))
            };

            const useCases = makeUseCases({gateways});

            return useCases
                .checkPhone(newPhone.phone)
                .then((doesPhoneExist) => expect(doesPhoneExist).to.eql(false));
        });
    });
});
