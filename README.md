# Phone checker

[![build status](https://gitlab.com/EvgenyOrekhov/phone-checker/badges/master/build.svg)](https://gitlab.com/EvgenyOrekhov/phone-checker/builds)
[![coverage report](https://gitlab.com/EvgenyOrekhov/phone-checker/badges/master/coverage.svg)](https://gitlab.com/EvgenyOrekhov/phone-checker/builds)

> Реализовать сервис для проверки наличия мобильного телефона в БД c
> использованием O-Auth2 авторизации. Для Front-end клиента использовать любой
> из удобных фреймверков. Состоит из поля для ввода и кнопки OK. Back-end:
> nodejs + express + pgsql. Account service (методы: login, logout) (в БД только
> один пользователь), Phone service (addPhone, removePhone, checkPhone).
> Back-office: Форма входа, вывод существующих номеров, добавление\удаление
> номеров. Все в произвольной форме (обработка ошибок, валидация данных и т.д.).
> Все в целях ознакомления с написанием кода и стеком используемых технологий.

![Back-office](screenshots/back-office.png)

## Services

- REST API:
  [http://localhost:8080/](http://localhost:8080/)
- Back-office:
  [http://localhost/back-office/](http://localhost/back-office/)
- Phone checker:
  [http://localhost/](http://localhost/)

## Deploy

```sh
docker-compose up
```

## Test

```sh
docker-compose -f docker-compose.test.yml run --rm test
```
